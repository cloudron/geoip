#!/bin/bash

set -eu

export NODE_ENV=production

touch /app/data/apitoken.txt

chown -R cloudron:cloudron /app/data

exec /usr/local/bin/gosu cloudron:cloudron node /app/code/index.js
