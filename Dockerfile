FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG MAXMIND_ACCOUNT_ID
ARG MAXMIND_LICENSE_KEY

RUN mkdir -p /app/code
WORKDIR /app/code

RUN curl -L -u ${MAXMIND_ACCOUNT_ID}:${MAXMIND_LICENSE_KEY} https://download.maxmind.com/geoip/databases/GeoLite2-City/download?suffix=tar.gz | tar zxvf - --strip-components 1 --wildcards GeoLite2-City_*/GeoLite2-City.mmdb

COPY package-lock.json package.json index.js start.sh /app/code/
COPY public /app/code/public
COPY logo.png /app/code/public/favicon.png

RUN npm install && npm cache clean --force

CMD [ "/app/code/start.sh" ]
