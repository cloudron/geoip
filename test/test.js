#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function checkApi() {
        const response = await fetch(`https://${app.fqdn}/json`);
        const body = await response.json();
        expect(body.city).to.be.ok();
    }

    xit('build app', () => execSync('cloudron build', EXEC_ARGS));
    it('install app', () => execSync('cloudron install --location ' + LOCATION, EXEC_ARGS));

    it('can get app ino', getAppInfo);

    it('can check api', checkApi);

    it('restart app', () => execSync('cloudron restart --app ' + app.id, EXEC_ARGS));
    it('can check api', checkApi);

    it('backup app', () => execSync('cloudron backup create --app ' + app.id, EXEC_ARGS));

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can check api', checkApi);

    it('uninstall app', () => execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS));

    // test update
    it('can install app', () => execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS));

    it('can get app information', getAppInfo);
    it('can check api', checkApi);

    it('can update', () => execSync('cloudron update --app ' + app.id, EXEC_ARGS));

    it('can check api', checkApi);

    it('uninstall app', () => execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS));
});
