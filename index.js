#!/usr/bin/env node

'use strict';

import fs from 'fs';
import cors from 'cors';
import express from 'express';
import maxmind from 'maxmind';

const gCityLookup = await maxmind.open('GeoLite2-City.mmdb');

function jsonp(req, res) {
    if (!req.query.callback) return res.status(400).send('jsonp requires callback query param');

    const ip = req.query.ip || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const city = gCityLookup.get(ip);

    res.status(200).send(req.query.callback + '(' + JSON.stringify(city) + ');');
}

function json(req, res) {
    const ip = req.query.ip || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const city = gCityLookup.get(ip);

    res.status(200).send(city || {});
}

function auth(req, res, next) {
    const TOKEN_FILE = '/app/data/apitoken.txt';
    if (!fs.existsSync(TOKEN_FILE)) return next();

    fs.readFile(TOKEN_FILE, { encoding: 'utf8' }, function (error, data) {
        if (error) return res.status(403).json({ error: `API token could not be read: ${error.message}` });

        const token = data.trim();
        if (!token || req.query.accessToken === token) return next();

        res.status(403).json({ error: 'API Token Required' });
    });
}

const app = express();
const router = new express.Router();

app.use(cors());
app.use(express.static('public'));
app.use(router);

app.get('/jsonp', auth, jsonp);
app.get('/json', auth, json);
app.get('/healthcheck', (req, res) => res.status(200).send({}));

app.listen(3000, function () {
    console.log('geoip app listening on port 3000');
});
