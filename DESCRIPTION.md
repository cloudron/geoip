A simple Geolocation service based on [maxmind](https://www.npmjs.com/package/maxmind).

### Usage

When making any request to either `/json` or `/jsonp?callback=functionName`, the service will
resolve the IP to a location and return as much information as possible about the country and city.

Additionally both routes can take a query param `ip` which will override the source ip.

